﻿USE [Northwind]
GO

SELECT *
FROM dbo.[Customers] as cust
WHERE EXISTS 
(
	SELECT ord.[CustomerID]
	FROM dbo.[Orders] as ord
	GROUP BY ord.[CustomerID]
	HAVING COUNT(*) = 0	
)
