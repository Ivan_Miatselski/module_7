﻿USE [Northwind]
GO

SELECT COUNT(
CASE
	WHEN ISNULL(ord.[ShippedDate], 0) = 0 THEN 1
END
) as 'Shipped Date'
FROM dbo.[Orders] as ord

