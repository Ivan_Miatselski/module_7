﻿USE [Northwind]
GO

SELECT ord.[EmployeeID], ord.[CustomerID], COUNT(*) as [Count Orders]
FROM dbo.[Orders] as ord
WHERE YEAR(ord.[ShippedDate]) = '1998'
GROUP BY ord.[EmployeeID], ord.[CustomerID]