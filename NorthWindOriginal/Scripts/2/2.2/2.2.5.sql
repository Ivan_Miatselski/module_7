﻿USE [Northwind]
GO

SELECT DISTINCT cust1.[City], cust2.[CustomerID]
FROM dbo.[Customers] as cust1, dbo.[Customers] as cust2
WHERE cust1.[CustomerID] <> cust2.[CustomerID] AND cust1.[City] = cust2.[City]
ORDER BY cust1.[City] DESC