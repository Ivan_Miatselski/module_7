﻿USE [Northwind]
GO

SELECT COUNT(*)
FROM dbo.[Orders]
GO

SELECT YEAR(ord.[OrderDate]) as [Year], COUNT(YEAR(ord.[OrderDate])) as [Total]
FROM dbo.[Orders] as ord
GROUP BY YEAR(ord.[OrderDate])