﻿USE [Northwind]
GO

SELECT cust.[CustomerID], COUNT(*) as [Count Orders]
FROM dbo.[Customers] as cust
JOIN dbo.[Orders] as ord ON ord.[CustomerID] = cust.[CustomerID]
GROUP BY cust.[CustomerID]
ORDER BY COUNT(*) ASC