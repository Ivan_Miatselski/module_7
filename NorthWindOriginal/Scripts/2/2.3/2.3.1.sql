﻿USE [Northwind]
GO

SELECT DISTINCT reg.[RegionDescription], emp.[FirstName], emp.[LastName] 
FROM dbo.[Region] as reg
JOIN dbo.[Territories] as ter ON reg.[RegionID] = ter.[RegionID]
JOIN dbo.[EmployeeTerritories] as emtr ON emtr.[TerritoryID] = ter.[TerritoryID]
JOIN dbo.[Employees] emp ON emp.[EmployeeID] = emtr.[EmployeeID]
WHERE reg.[RegionDescription] = 'Western'