﻿USE [Northwind]
GO

SELECT cust.[CustomerID], cust.[Country]
FROM dbo.[Customers] as cust
WHERE LEFT(cust.[Country], 1) >= 'b' AND LEFT(cust.[Country], 1) <= 'g'