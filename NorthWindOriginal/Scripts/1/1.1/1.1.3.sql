﻿USE [Northwind]
GO

SELECT ord.[OrderID] as 'Order Number', 
CASE
	WHEN ISNULL(ord.[ShippedDate], 0) = 0 THEN 'Not Shipped'
	ELSE CAST(ord.[ShippedDate] as nvarchar)
END as 'Shipped Date'
FROM dbo.[Orders] as ord
WHERE ord.[ShippedDate] > '6 May, 1998' OR ord.[ShippedDate] is NULL