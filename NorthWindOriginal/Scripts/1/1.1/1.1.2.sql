﻿USE [Northwind]
GO

SELECT ord.[OrderID], 
CASE
	WHEN ISNULL(ord.[ShippedDate], 0) = 0 THEN 'Not shipped'
END AS [ShippedDate]
FROM dbo.[Orders] as ord
WHERE ord.[ShippedDate] is NULL