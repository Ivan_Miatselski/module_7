﻿USE [Northwind]
GO

SELECT cust.[ContactName], cust.[Country]
FROM dbo.[Customers] as cust
WHERE cust.[Country] IN ('USA', 'Canada')
ORDER BY cust.[ContactName], cust.[Country]